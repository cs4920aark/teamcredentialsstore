Team Credentials Store

Your assignment is to create a Credentials store for Team based accounts.

Basic requirements are by a 'group' - which would be a team of developers, store common credentials account and password encrypted.  Commerce currently uses Windows AD Domain for groups. You can use the simplest method to define users and groups.

Only those individuals that are in a group have access to update and read any of the credentials belonging to that group.

The credentials need to be SHA256 bit encrypted with a unique SALT per set of credentials. The SALT may be stored with the credentials.

You will need to establish multiple users in multiple groups to test and validate.

A fairly simple UI is required to List, Add, Update and Delete entries.

Also a REST Service will be needed to return a password for a specific account. This needs to be using the HTTP Post method only. No other HTTP methods will be supported for the REST Service. All data will be passed via JSON with the exception of the authentication - which can be simple HTTP Header with base 64 encoding.

You will need to use the latest of:
	Java 8
	Spring Framework
	Maven
	MySQL or Maria DB


Example Database Table

create table credentials {
	oid MEDIUMINT NOT NULL AUTO_INCREMENT,
	group			varchar(256) not null,
	account			varchar(256) not null,
	password		varchar(512) not null,
	salt			varchar(256) not null,
	create_user		varchar(256) not null,
	create_timestamp 	timestamp not null,
	primary key(id)
}
