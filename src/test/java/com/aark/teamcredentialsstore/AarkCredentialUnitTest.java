/*
package com.aark.teamcredentialsstore;

import com.aark.teamcredentialsstore.CredentialRepository;
import com.aark.teamcredentialsstore.AarkCredential;
import org.apache.tomcat.jni.User;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.internal.verification.VerificationModeFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.ldap.embedded.EmbeddedLdapProperties;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class AarkCredentialUnitTest {

    @Rule
    public AarkCredential restDocumentation = new AarkCredential();

    private MockMvc mockMvc;
    private AarkCredential aarkCredential;

    @MockBean
    private CredentialRepository credentialRepository;

    @Autowired
    private CredentialController controller;

    @Before
    public void setup(){
        mockMvc = MockMvcBuilders.standaloneSetup(controller).build();
        AarkCredential = new aarkCredential();
        AarkCredential.setDeveloperGroup("Abdul");
        AarkCredential.setPassword("asasasdss");
        when(credentialRepository.findByDeveloperGroup("Abdul")).thenReturn(AarkCredential);

    }

    @Test
    public void contexLoads() {
        assertThat(controller).isNotNull();
    }




    @Test
    public void testIndex() throws Exception{
        this.mockMvc.perform(get("/api/"))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("index")))
                .andDo(print())
                .andDo(document("api"))
        ;
    }

    //Test UserController
    @Test
    public void testGetUser() throws Exception{

        this.mockMvc.perform(get("/api/group/1"))
                //.contentType(MediaType.APPLICATION_JSON)
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.name",is(user1.getName())))
                .andDo(print())
                .andDo(document("user", responseFields(
                        fieldWithPath("group")
                                .description("The user's group"),
                        fieldWithPath("name").description("The user's name"),
                        fieldWithPath("id").description("The user's id"))))



        ;
    }



    @Test
    public void testGetAllUser() throws Exception{
        User user2=new User();
        user2.setId(11);
        user2.setAccount("1110101");
        user2.setDeveloperGroup("group1");
        List<EmbeddedLdapProperties.Credential> credentialList = Arrays.asList(user1, user2);
        given(credentialRepository.findAll()).willReturn(allUsers);
        this.mockMvc.perform(get("/api/all"))

                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", hasSize(2)))

                .andExpect(jsonPath("$[0].group",is(user1.getDeveloperGroup())))
                .andExpect(jsonPath("$[1].group",is(user2.getDeveloperGroup())))
                .andDo(print());
        verify(credentialRepository, VerificationModeFactory.times(1)).findAll();
    }

}
*/
