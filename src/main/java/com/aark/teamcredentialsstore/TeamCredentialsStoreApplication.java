package com.aark.teamcredentialsstore;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TeamCredentialsStoreApplication {
	public static void main(String[] args) {
		SpringApplication.run(TeamCredentialsStoreApplication.class, args);
	}
}
