package com.aark.teamcredentialsstore.model;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "aark_credential",
        uniqueConstraints = @UniqueConstraint(columnNames = {"userGroup", "account"}))
public class Credential {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    private Long userGroup;

    @NotNull
    @Size(max = 256)
    private String account;

    @NotNull
    @Size(max = 512)
    private String password;

    @NotNull
    @Size(max = 256)
    private String createUser;

    @NotNull
    @Temporal(TemporalType.TIMESTAMP)
    private Date createTimeStamp;

    @ManyToMany(mappedBy = "credentials", cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    private Set<Group> groups = new HashSet<>();

    public Credential() {}

    public Credential(Long userGroup, String account, String password, String createUser, Date createTimeStamp) {
        this.userGroup = userGroup;
        this.account = account;
        this.password = password;
        this.createUser = createUser;
        this.createTimeStamp = createTimeStamp;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserGroup() {
        return userGroup;
    }

    public void setUserGroup(Long userGroup) {
        this.userGroup = userGroup;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public Date getCreateTimeStamp() {
        return createTimeStamp;
    }

    public void setCreateTimeStamp(Date createTimeStamp) {
        this.createTimeStamp = createTimeStamp;
    }
}