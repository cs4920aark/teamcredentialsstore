package com.aark.teamcredentialsstore.repository;

import com.aark.teamcredentialsstore.model.Group;
import com.aark.teamcredentialsstore.model.User;
import org.springframework.data.repository.CrudRepository;

public interface GroupRepository extends CrudRepository<Group, Long> {
    Group findOneByName(String name);
    Iterable<Group> findByUsersContains(User user);
    Iterable<Group> findByUsersNotContains(User user);
}
