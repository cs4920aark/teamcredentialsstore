package com.aark.teamcredentialsstore.repository;

import com.aark.teamcredentialsstore.model.Credential;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface CredentialRepository extends CrudRepository<Credential, Long> {
    @Query(value = "SELECT * FROM aark_credential ORDER BY user_group ASC, account ASC", nativeQuery = true)
    Iterable<Credential> findAll();
    Credential findOneById(Long id);
}