package com.aark.teamcredentialsstore.repository;

import com.aark.teamcredentialsstore.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Long> {
    User findByUsername(String username);
}
