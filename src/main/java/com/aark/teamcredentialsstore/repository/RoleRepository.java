package com.aark.teamcredentialsstore.repository;

import com.aark.teamcredentialsstore.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepository extends JpaRepository<Role, Long>{
}
