package com.aark.teamcredentialsstore.web;

import java.security.Principal;
import java.util.Base64;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import com.aark.teamcredentialsstore.model.Credential;
import com.aark.teamcredentialsstore.model.Group;
import com.aark.teamcredentialsstore.model.User;
import com.aark.teamcredentialsstore.repository.CredentialRepository;
import com.aark.teamcredentialsstore.repository.GroupRepository;
import com.aark.teamcredentialsstore.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.web.bind.annotation.*;
import org.springframework.context.annotation.Configuration;

@Configuration
@RestController
@RequestMapping(path = "/tcs")
public class CredentialController {
    @Autowired private CredentialRepository credentialRepository;
    @Autowired private UserRepository userRepository;
    @Autowired private GroupRepository groupRepository;

    @RequestMapping(path = "/create-group", method = RequestMethod.POST, produces = "application/json")
    public String createGroup(@RequestParam("groupName") String groupName, Principal principal) {
        String message;
        User user = this.userRepository.findByUsername(principal.getName());
        if(!groupName.matches("[a-zA-Z0-9]{1,255}")){
            message = "Group name '" + groupName + "' is invalid.";
        } else if(this.groupRepository.findOneByName(groupName) != null) {
            message = "Group '" + groupName + "' already exists.";
        } else if(user == null) {
            message = "Username '" + principal.getName() + "' is invalid.";
        } else {
            Group group = new Group(groupName);
            Set<User> users = group.getUsers();
            users.add(user);
            group.setUsers(users);
            try {
                this.groupRepository.save(group);
                message = "Group '" + groupName + "' saved.";
            } catch (DataAccessException dae) {
                message = "An error occurred while attempting to save.";
            }
        }
        return "{\"message\":\"" + message + "\"}";
    }

    @RequestMapping(path = "/join-group", method = RequestMethod.POST, produces = "application/json")
    public String joinGroup(@RequestParam("groupName") String groupName, Principal principal) {
        String message;
        if(!groupName.matches("[a-zA-Z0-9]{1,255}")){
            message = "Group name '" + groupName + "' is invalid.";
        } else {
            Group group = this.groupRepository.findOneByName(groupName);
            User user = this.userRepository.findByUsername(principal.getName());
            if(group == null) {
                message = "Group name '" + groupName + "' is invalid.";
            } else if(user == null) {
                message = "Username '" + principal.getName() + "' is invalid.";
            } else {
                Set<User> users = group.getUsers();
                if(users.contains(user)) {
                    message = "You already belong to '" + groupName + "'.";
                } else {
                    users.add(user);
                    group.setUsers(users);
                    try {
                        this.groupRepository.save(group);
                        message = "Joined group '" + groupName + "'.";
                    } catch (DataAccessException dae) {
                        message = "An error occurred while attempting to save.";
                    }
                }
            }
        }
        return "{\"message\":\"" + message + "\"}";
    }

    @RequestMapping(path = "/get-foreign-groups", method = RequestMethod.POST, produces = "application/json")
    public Iterable<String> getAllGroups(Principal principal) {
        User user = this.userRepository.findByUsername(principal.getName());
        if(user == null) {
            return null;
        }
        Iterable<Group> groups = this.groupRepository.findByUsersNotContains(user);
        if(groups == null) {
            return null;
        }
        Set<String> groupNames = new HashSet<>();
        for(Group group : groups) {
            groupNames.add(group.getName());
        }
        return groupNames;
    }

    @RequestMapping(path = "/get-user-credentials", method = RequestMethod.POST, produces = "application/json")
    public Iterable<Group> getUserCredentials(Principal principal) {
        User user = this.userRepository.findByUsername(principal.getName());
        if(user == null) {
            return null;
        }
        Iterable<Group> groups = this.groupRepository.findByUsersContains(user);
        if(groups == null) {
            return null;
        }
        for(Group group : groups) {
            for(Credential credential : group.getCredentials()) {
                String encodedPassword = credential.getPassword();
                byte[] decodedBytes = Base64.getDecoder().decode(encodedPassword);
                credential.setPassword(new String(decodedBytes));
            }
        }
        return groups;
    }

    @RequestMapping(path = "/create-credential", method = RequestMethod.POST, produces = "application/json")
    public String createCredential(
            @RequestParam("account") String account,
            @RequestParam("password") String password,
            @RequestParam("groupName") String groupName,
            Principal principal) {
        String message;
        if(!groupName.matches("[a-zA-Z0-9]{1,255}")){
            message = "Group name '" + groupName + "' is invalid.";
        } else if(!account.matches("[a-zA-Z0-9]{1,256}")) {
            message = "Account name '" + account + "' is invalid.";
        } else if(!password.matches("[a-zA-Z0-9!@#$%^&*()]{1,64}")) {
            message = "Password is invalid.";
        } else {
            Group group = this.groupRepository.findOneByName(groupName);
            User user = this.userRepository.findByUsername(principal.getName());
            if(group == null) {
                message = "Group name '" + groupName + "' is invalid.";
            } else if(!group.getUsers().contains(user)) {
                message = "You are not a member of '" + groupName + "'.";
            } else {
                String encodedPassword = Base64.getEncoder().encodeToString(password.getBytes());
                Credential credential = new Credential(group.getId(), account, encodedPassword, user.getUsername(), new Date());
                Set<Credential> credentials = group.getCredentials();
                credentials.add(credential);
                try {
                    this.credentialRepository.save(credential);
                    this.groupRepository.save(group);
                    message = "Credential for account '" + account + "' saved.";
                } catch(DataAccessException dae) {
                    message = "An error occurred while attempting to save.";
                }
            }
        }
        return "{\"message\":\"" + message + "\"}";
    }

    @RequestMapping(path = "/update-credential", method = RequestMethod.POST, produces = "application/json")
    public String updateCredential(
            @RequestParam("account") String account,
            @RequestParam("password") String password,
            @RequestParam("credentialId") Long credentialId) {
        String message;
        Credential credential = this.credentialRepository.findOneById(credentialId);
        if(credential == null) {
            message = "Credential for account '" + account + "' not found.";
        } else if(!account.matches("[a-zA-Z0-9]{1,256}")) {
            message = "Account name '" + account + "' is invalid.";
        } else if(!password.matches("[a-zA-Z0-9!@#$%^&*()]{1,64}")) {
            message = "Password is invalid.";
        } else {
            credential.setAccount(account);
            String encodedPassword = Base64.getEncoder().encodeToString(password.getBytes());
            credential.setPassword(encodedPassword);
            try {
                this.credentialRepository.save(credential);
                message = "Credential for account '" + account + "' updated.";
            } catch(DataAccessException dae) {
                message = "An error occurred while attempting to save.";
            }
        }
        return "{\"message\":\"" + message + "\"}";
    }

    @RequestMapping(path = "/delete-credential", method = RequestMethod.POST, produces = "application/json")
    public String deleteCredential(
            @RequestParam("account") String account,
            @RequestParam("credentialId") Long credentialId) {
        String message;
        Credential credential = this.credentialRepository.findOneById(credentialId);
        if(credential == null) {
            message = "Credential for account '" + account + "' not found.";
        } else if(!account.matches("[a-zA-Z0-9]{1,256}")) {
            message = "Account name '" + account + "' is invalid.";
        } else {
            try {
                this.credentialRepository.delete(credential);
                message = "Credential for account '" + account + "' deleted.";
            } catch(DataAccessException dae) {
                message = "An error occurred while attempting to delete.";
            }
        }
        return "{\"message\":\"" + message + "\"}";
    }
}
