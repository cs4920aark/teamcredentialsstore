<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="${contextPath}/resources/css/common.css" rel="stylesheet">
        <link href="${contextPath}/resources/css/login.css" rel="stylesheet">
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
        <title>TCS - Log in</title>
    </head>
    <body>
        <header>Team Credentials Store</header>
        <form method="POST" action="${contextPath}/login" class="login-form">
            <h2>Log in</h2>
            <span class="${error != null ? 'error' : ''}">${message}</span>
            <input name="username" type="text" placeholder="Username" autofocus="true" required />
            <input name="password" type="password" placeholder="Password" required />
            <span class="error">${error}</span>
            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
            <input type="submit" value="Log In" />
            <h4 class="text-center">New here? <a href="${contextPath}/registration">Create an account</a></h4>
        </form>
    </body>
</html>
