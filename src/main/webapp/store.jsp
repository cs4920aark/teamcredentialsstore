<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="_csrf" content="${_csrf.token}"/>
        <meta name="_csrf_header" content="${_csrf.headerName}"/>
        <link href="${contextPath}/resources/css/common.css" rel="stylesheet">
        <link href="${contextPath}/resources/css/store.css" rel="stylesheet">
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
        <title>Team Credentials Store</title>
    </head>
    <body>
        <div class="container">
            <c:if test="${pageContext.request.userPrincipal.name != null}">
                <header>Team Credentials Store</header>
                <form id="logout-form" method="POST" action="${contextPath}/logout">
                    <span class="active-username">${pageContext.request.userPrincipal.name}</span>
                    <input type="submit" value="Log out" />
                    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                </form>
                <div class="add-new">
                    <form method="POST" onsubmit="createGroup(this)">
                        Create new group
                        <input name="group-name" type="text" placeholder="Group Name" required>
                        <input type="submit" value="Create">
                    </form>
                    <b>OR</b>
                    <form method="POST" onsubmit="joinGroup(this)">
                        Join existing group
                        <select name="group-name" id="group-select" required></select>
                        <input type="submit" value="Join">
                    </form>
                </div>
                <div class="wrapper"></div>
                <div id="status"></div>
            </c:if>
        </div>
    </body>
    <script type="text/javascript">
        var token, header;

        $(function() {
            token = $("meta[name='_csrf']").attr("content");
            header = $("meta[name='_csrf_header']").attr("content");
            populateCredentialList();
            populateGroupList();
        });

        function populateGroupList() {
            event.preventDefault();
            var ajaxOptions = new Object();
            ajaxOptions.dataType = 'text';
            ajaxOptions.type = 'POST';
            ajaxOptions.url = "./tcs/get-foreign-groups";
            ajaxOptions.beforeSend = function(xhr) {
                xhr.setRequestHeader(header, token);
            };
            ajaxOptions.success = function(response) {
                resetSelect("group-select");
                var groups = $.parseJSON(response);
                var groupSelect = document.getElementById("group-select");
                var groupOption;
                $.each(groups, function(index, value) {
                    groupOption = document.createElement('option');
                    groupOption.value = value;
                    groupOption.innerHTML = value;
                    groupSelect.appendChild(groupOption);
                });
            };
            $.ajax(ajaxOptions);
        }

        function populateCredentialList() {
            var ajaxOptions = new Object();
            ajaxOptions.dataType = 'text';
            ajaxOptions.type = 'POST';
            ajaxOptions.url = './tcs/get-user-credentials';
            ajaxOptions.beforeSend = function(xhr) {
                xhr.setRequestHeader(header, token);
            };
            ajaxOptions.success = function(response) {
                var groups = jQuery.parseJSON(response);
                console.log(groups);
                var groupsHtml = "";
                $.each(groups, function(index, value) {
                    groupsHtml += "<div class=\"group\" id=\"groupname\">\n" +
                        "               <div class=\"group-header\">" + value.name + "</div>\n" +
                        "                <div class=\"add-new \">\n" +
                        "                    <form method=\"POST\" onsubmit=\"createCredential(this)\">\n" +
                        "                        <input name=\"account\" type=\"text\" placeholder=\"Account\" required>\n" +
                        "                        <input name=\"password\" type=\"password\" placeholder=\"Password\" autocomplete=\"new-password\" required>\n" +
                        "                        <input type=\"checkbox\" onclick=\"togglePasswordVisibility(this)\">Show Password\n" +
                        "                        <input type=\"submit\" value=\"Add\">\n" +
                        "                        <input name=\"group-name\" type=\"hidden\" value=\"" + value.name + "\">\n" +
                        "                    </form>\n";
                    $.each(value.credentials, function(index, value){
                        groupsHtml += "<form method=\"POST\" onsubmit=\"updateDeleteCredential(this)\">\n" +
                        "                        <input name=\"account\" type=\"text\" value=\"" + value.account + "\" required>\n" +
                        "                        <input name=\"password\" type=\"password\" value=\"" + value.password + "\" autocomplete=\"new-password\" required>\n" +
                        "                        <input type=\"checkbox\" onclick=\"togglePasswordVisibility(this)\">Show Password\n" +
                        "                        <span class=\"read-only-field\">" + value.createUser + "</span>\n" +
                        "                        <span class=\"read-only-field\">" + value.createTimeStamp + "</span>\n" +
                        "                        <input type=\"submit\" value=\"Delete\">\n" +
                        "                        <input type=\"submit\" value=\"Update\">\n" +
                        "                        <input type=\"hidden\" name=\"credential-id\" value=\"" + value.id + "\">\n" +
                        "                    </form>\n";
                    });
                    groupsHtml += "</div>\n" +
                        "</div>";

                });
                $('.wrapper').first().html(groupsHtml);
            };
            $.ajax(ajaxOptions);
        }

        function createCredential(form) {
            event.preventDefault();
            var formData = new Object();
            formData.account = form.elements['account'].value;
            formData.password = form.elements['password'].value;
            formData.groupName = form.elements['group-name'].value;
            var ajaxOptions = new Object();
            ajaxOptions.data = formData;
            ajaxOptions.dataType = 'text';
            ajaxOptions.type = 'POST';
            ajaxOptions.url = "./tcs/create-credential";
            ajaxOptions.beforeSend = function(xhr) {
                xhr.setRequestHeader(header, token);
            };
            ajaxOptions.success = function(response) {
                var responseObject = $.parseJSON(response);
                var statusField = $('#status');
                statusField.text(responseObject.message);
                //clear fields and refresh list data***
                populateCredentialList();
                populateGroupList();
                form.reset();
            };
            $.ajax(ajaxOptions);
        }

        function updateDeleteCredential(form) {
            event.preventDefault();
            var formData = new Object();
            var targetUrl;
            var selectedAction = document.activeElement.value;
            switch(selectedAction){
                case 'Delete':
                    formData.account = form.elements['account'].value;
                    formData.credentialId = form.elements['credential-id'].value;
                    targetUrl = './tcs/delete-credential';
                    break;
                case 'Update':
                    formData.account = form.elements['account'].value;
                    formData.password = form.elements['password'].value;
                    formData.credentialId = form.elements['credential-id'].value;
                    targetUrl = './tcs/update-credential';
                    break;
                default:
                    alert('Form submission error');
                    return;
            }
            var ajaxOptions = new Object();
            ajaxOptions.data = formData;
            ajaxOptions.dataType = 'text';
            ajaxOptions.type = 'POST';
            ajaxOptions.url = targetUrl;
            ajaxOptions.beforeSend = function(xhr) {
                xhr.setRequestHeader(header, token);
            };
            ajaxOptions.success = function(response) {
                var responseObject = $.parseJSON(response);
                var statusField = $('#status');
                statusField.text(responseObject.message);
                populateCredentialList();
                form.reset();
            };
            $.ajax(ajaxOptions);
        }

        function createGroup(form) {
            event.preventDefault();
            var formData = new Object();
            formData.groupName = form.elements['group-name'].value;
            var ajaxOptions = new Object();
            ajaxOptions.data = formData;
            ajaxOptions.dataType = 'text';
            ajaxOptions.type = 'POST';
            ajaxOptions.url = "./tcs/create-group";
            ajaxOptions.beforeSend = function(xhr) {
                xhr.setRequestHeader(header, token);
            };
            ajaxOptions.success = function(response) {
                var responseObject = $.parseJSON(response);
                var statusField = $('#status');
                statusField.text(responseObject.message);
                populateCredentialList();
                populateGroupList();
                form.reset();
            };
            $.ajax(ajaxOptions);
        }

        function joinGroup(form) {
            event.preventDefault();
            var formData = new Object();
            formData.groupName = form.elements['group-name'].value;
            var ajaxOptions = new Object();
            ajaxOptions.data = formData;
            ajaxOptions.dataType = 'text';
            ajaxOptions.type = 'POST';
            ajaxOptions.url = "./tcs/join-group";
            ajaxOptions.beforeSend = function(xhr) {
                xhr.setRequestHeader(header, token);
            };
            ajaxOptions.success = function(response) {
                var responseObject = $.parseJSON(response);
                var statusField = $('#status');
                statusField.text(responseObject.message);
                populateGroupList();
                populateCredentialList();
                form.reset();
            };
            $.ajax(ajaxOptions);
        }

        function resetSelect(elementId) {
            var groupSelect = document.getElementById(elementId);
            for(var i = groupSelect.options.length - 1 ; i >= 0 ; i--) {
                groupSelect.remove(i);
            }
        }

        function togglePasswordVisibility(toggle) {
            var passwordField = toggle.parentNode.elements['password'];
            passwordField.type = (passwordField.type === 'password' ? 'text' : 'password');
        }
    </script>
</html>
